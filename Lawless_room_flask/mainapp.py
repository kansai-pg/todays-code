import sqlite3
import datetime
import bleach
from flask import Flask,render_template,request,redirect

def get_from_sql():

    sqlselect = []
    conn = sqlite3.connect("chat")
    cur = conn.cursor()

    try:
        cur.execute("create table mytable(comment text, updated datetime);")
    except sqlite3.OperationalError:
        pass

    cur.execute('SELECT * FROM mytable order by updated desc;')

    for row in cur:
        sqlselect.append(row)

    conn.close

    return sqlselect

def insert_for_sql(write):

    conn = sqlite3.connect("chat")
    conn.cursor().execute("insert into mytable(comment, updated) values (?,?)",(write, datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")))
    conn.commit()
    conn.close

    return

app = Flask(__name__)

@app.route('/')
def test():
    return render_template('mainpage.html', message=get_from_sql())

@app.route('/write')
def write_content():
    content = request.args.get('text')
    if not content.isspace():
        insert_for_sql(bleach.clean(content))
    return redirect('/')

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=8000)